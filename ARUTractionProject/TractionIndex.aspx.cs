﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.IO;
using BusinessLogicLayer;



namespace ARUTractionProject
{
    public partial class TractionIndex : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) { 
            var ob = new BusinessLogicLayer.TeamDriveData();
            IList<Google.Apis.Drive.v3.Data.File> sheetsList = ob.RetrieveSpreadheetsDetails();
            ob.ProcessSpreadsheetDetails(sheetsList);
            
            DDLSpreadsheets.DataSource = sheetsList;
            DDLSpreadsheets.DataTextField = "Name";
            DDLSpreadsheets.DataValueField = "Id";
            DDLSpreadsheets.DataBind();
            DDLSpreadsheets.Items.Insert(0, new ListItem("--Please select the spreadsheet--", "0"));

            }
        //ob.GetReportData();





    }
        protected void RefreshDDLSpreadsheetsData(object sender, EventArgs e)
        {
            var ob = new BusinessLogicLayer.TeamDriveData();
            IList<Google.Apis.Drive.v3.Data.File> sheetsList = ob.RetrieveSpreadheetsDetails();
            ob.ProcessSpreadsheetDetails(sheetsList);

            DDLSpreadsheets.DataSource = sheetsList;
            DDLSpreadsheets.DataTextField = "Name";
            DDLSpreadsheets.DataValueField = "Id";
            DDLSpreadsheets.DataBind();
            DDLSpreadsheets.Items.Insert(0, new ListItem("--Please select the spreadsheet--", "0"));
        }
        protected void DDLSpreadsheets_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DDLSpreadsheets.SelectedValue != "0") {
                Honeycomb db = new Honeycomb();
                var ob = new BusinessLogicLayer.SheetsData();
                int id = ob.RetrieveSpreadheetsData(DDLSpreadsheets.SelectedValue);
                var result = (from HDE in db.HoneycombDashboardEmployees
                              join HDD in db.HoneycombDashboardDatas
                              on HDE.ID equals HDD.HonecombDashboardEmployee_ID
                              where HDE.SpreadsheetID == id
                              select new
                              {
                                  HDE.FirstName,
                                  HDE.LastName,
                                  HDE.Initials,
                                  HDE.FTStatus,
                                  HDE.Status,
                                  HDE.DateJoined,
                                  HDE.EndOfTrimester,
                                  HDD.Month,
                                  HDD.BillableHours,
                                  HDD.Credits,
                                  HDD.GoalBillableHours,
                                  HDD.Reminders,
                                  HDD.SpecialNotes
                              }).ToList();



                GridView1.DataSource = result;
                GridView1.AutoGenerateColumns = true;
                GridView1.DataBind();
                DDLSpreadsheets.SelectedIndex = 0;
                
            }
        }
    }
}