﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TractionIndex.aspx.cs" Inherits="ARUTractionProject.TractionIndex" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" >

    </link>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="jumbotron">
        <div class="container">
          <h1 class="display-3">Arturo Ruiz traction project</h1>
          <p>This is an example on how to use Google Drive and Google Sheets. It queries the Google spreadsheet data out of the Honeycomb Team Drive and displays it on this example page</p>
          
        </div>
      </div>
        
        <div class="cointainer">
            
            <p></p>
        </div>
        <div class="container">
            <p>To begin, select a Spreadsheet from the menu below</p>
            <asp:DropDownList ID="DDLSpreadsheets" OnSelectedIndexChanged="DDLSpreadsheets_SelectedIndexChanged"  runat="server" AutoPostBack="True">
            </asp:DropDownList>
            <asp:Button ID="btnSync" OnClick="RefreshDDLSpreadsheetsData" CssClass="btn btn-primary btn-lg" runat="server" Text="Sync Sheets" />
        </div>
        <div>   
            
            <asp:GridView ID="GridView1" CssClass="table table-hover"  runat="server"></asp:GridView>
        </div>
    </form>
</body>
</html>
