namespace BusinessLogicLayer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HoneycombDashboardData")]
    public partial class HoneycombDashboardData
    {
        public int ID { get; set; }

        public int HonecombDashboardEmployee_ID { get; set; }

        public int? SpreadsheetID { get; set; }

        [Required]
        [StringLength(15)]
        public string Month { get; set; }

        public decimal? BillableHours { get; set; }

        public decimal? Credits { get; set; }

        public decimal? GoalBillableHours { get; set; }

        public int? Reminders { get; set; }

        [StringLength(200)]
        public string SpecialNotes { get; set; }

        public virtual HoneycombDashboardEmployee HoneycombDashboardEmployee { get; set; }
    }
}
