namespace BusinessLogicLayer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Log")]
    public partial class Log
    {
        public int ID { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Message { get; set; }

        [StringLength(50)]
        public string HoneyCombUser { get; set; }
    }
}
