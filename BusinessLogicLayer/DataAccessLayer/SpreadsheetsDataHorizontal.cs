namespace BusinessLogicLayer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SpreadsheetsDataHorizontal")]
    public partial class SpreadsheetsDataHorizontal
    {
        public int ID { get; set; }

        public int SpreadsheetID { get; set; }

        public int RowNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string SheetID { get; set; }

        public int SpreadsheetColumn { get; set; }

        [StringLength(100)]
        public string Value { get; set; }

        public virtual Spreadsheet Spreadsheet { get; set; }
    }
}
