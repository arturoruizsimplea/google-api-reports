namespace BusinessLogicLayer
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Honeycomb : DbContext
    {
        public Honeycomb()
            : base("name=Honeycomb")
        {
        }

        public virtual DbSet<HoneycombDashboardData> HoneycombDashboardDatas { get; set; }
        public virtual DbSet<HoneycombDashboardEmployee> HoneycombDashboardEmployees { get; set; }
        public virtual DbSet<Log> Logs { get; set; }
        public virtual DbSet<Spreadsheet> Spreadsheets { get; set; }
        public virtual DbSet<SpreadsheetsDataHorizontal> SpreadsheetsDataHorizontals { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<HoneycombDashboardData>()
                .Property(e => e.Month)
                .IsUnicode(false);

            modelBuilder.Entity<HoneycombDashboardData>()
                .Property(e => e.BillableHours)
                .HasPrecision(10, 2);

            modelBuilder.Entity<HoneycombDashboardData>()
                .Property(e => e.Credits)
                .HasPrecision(10, 2);

            modelBuilder.Entity<HoneycombDashboardData>()
                .Property(e => e.GoalBillableHours)
                .HasPrecision(10, 2);

            modelBuilder.Entity<HoneycombDashboardData>()
                .Property(e => e.SpecialNotes)
                .IsUnicode(false);

            modelBuilder.Entity<HoneycombDashboardEmployee>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<HoneycombDashboardEmployee>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<HoneycombDashboardEmployee>()
                .Property(e => e.Initials)
                .IsUnicode(false);

            modelBuilder.Entity<HoneycombDashboardEmployee>()
                .Property(e => e.EmailAddress)
                .IsUnicode(false);

            modelBuilder.Entity<HoneycombDashboardEmployee>()
                .Property(e => e.FTStatus)
                .IsUnicode(false);

            modelBuilder.Entity<HoneycombDashboardEmployee>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<HoneycombDashboardEmployee>()
                .Property(e => e.DateJoined)
                .IsUnicode(false);

            modelBuilder.Entity<HoneycombDashboardEmployee>()
                .Property(e => e.EndOfTrimester)
                .IsUnicode(false);

            modelBuilder.Entity<HoneycombDashboardEmployee>()
                .HasMany(e => e.HoneycombDashboardDatas)
                .WithRequired(e => e.HoneycombDashboardEmployee)
                .HasForeignKey(e => e.HonecombDashboardEmployee_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Log>()
                .Property(e => e.Message)
                .IsUnicode(false);

            modelBuilder.Entity<Log>()
                .Property(e => e.HoneyCombUser)
                .IsUnicode(false);

            modelBuilder.Entity<Spreadsheet>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Spreadsheet>()
                .Property(e => e.SheetID)
                .IsUnicode(false);

            modelBuilder.Entity<Spreadsheet>()
                .Property(e => e.Location)
                .IsUnicode(false);

            modelBuilder.Entity<Spreadsheet>()
                .HasMany(e => e.HoneycombDashboardEmployees)
                .WithRequired(e => e.Spreadsheet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Spreadsheet>()
                .HasMany(e => e.SpreadsheetsDataHorizontals)
                .WithRequired(e => e.Spreadsheet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SpreadsheetsDataHorizontal>()
                .Property(e => e.SheetID)
                .IsUnicode(false);

            modelBuilder.Entity<SpreadsheetsDataHorizontal>()
                .Property(e => e.Value)
                .IsUnicode(false);
        }
    }
}
