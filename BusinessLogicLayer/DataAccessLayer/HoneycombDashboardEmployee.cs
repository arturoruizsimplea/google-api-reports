namespace BusinessLogicLayer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HoneycombDashboardEmployee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HoneycombDashboardEmployee()
        {
            HoneycombDashboardDatas = new HashSet<HoneycombDashboardData>();
        }

        public int ID { get; set; }

        public int SpreadsheetID { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [StringLength(4)]
        public string Initials { get; set; }

        [Required]
        [StringLength(20)]
        public string EmailAddress { get; set; }

        [Required]
        [StringLength(3)]
        public string FTStatus { get; set; }

        [Required]
        [StringLength(15)]
        public string Status { get; set; }

        [Required]
        [StringLength(10)]
        public string DateJoined { get; set; }

        [Required]
        [StringLength(10)]
        public string EndOfTrimester { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HoneycombDashboardData> HoneycombDashboardDatas { get; set; }

        public virtual Spreadsheet Spreadsheet { get; set; }
    }
}
