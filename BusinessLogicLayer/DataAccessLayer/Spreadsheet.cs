namespace BusinessLogicLayer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Spreadsheet
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Spreadsheet()
        {
            HoneycombDashboardEmployees = new HashSet<HoneycombDashboardEmployee>();
            SpreadsheetsDataHorizontals = new HashSet<SpreadsheetsDataHorizontal>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(75)]
        public string Name { get; set; }

        [Required]
        [StringLength(75)]
        public string SheetID { get; set; }

        [StringLength(100)]
        public string Location { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HoneycombDashboardEmployee> HoneycombDashboardEmployees { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpreadsheetsDataHorizontal> SpreadsheetsDataHorizontals { get; set; }
    }
}
