﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.IO;
using System.Data.SqlClient;
using System.Data.Sql;



namespace BusinessLogicLayer
{
    public class TeamDriveData
    {
        Honeycomb db = new Honeycomb();
            /*// If modifying these scopes, delete your previously saved credentials
        // at ~/.credentials/drive-dotnet-quickstart.json
        static string[] Scopes = { DriveService.Scope.DriveReadonly };
        static string ApplicationName = "ARU Traction Project";

        public UserCredential CreateConnection() {
            UserCredential credential;

            using (var stream =
                new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, ".credentials/test_google_team_drive.json");

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                
            }

            return credential;
            
        }
        */
        public void GetReportData()
        {
            var result = (from HDE in db.HoneycombDashboardEmployees
             join HDD in db.HoneycombDashboardDatas
             on HDE.ID equals HDD.HonecombDashboardEmployee_ID
             select new
             {
                 HDE.FirstName,
                 HDE.LastName,
                 HDE.Initials,
                 HDE.FTStatus,
                 HDE.Status,
                 HDE.DateJoined,
                 HDE.EndOfTrimester,
                 HDD.Month,
                 HDD.BillableHours,
                 HDD.Credits,
                 HDD.GoalBillableHours,
                 HDD.Reminders,
                 HDD.SpecialNotes
             }).ToList();
            int x = 0;
            

        }
        public IList<Google.Apis.Drive.v3.Data.File> RetrieveSpreadheetsDetails()
        {
            // Create Drive API service.
            var service = APIConnection.CreateDriveService();


            // Define parameters of request.
            FilesResource.ListRequest listRequest = service.Files.List();
            listRequest.PageSize = 10;
            listRequest.Fields = "nextPageToken, files(id, name, parents)";
            listRequest.Corpora = "teamDrive";
            listRequest.IncludeTeamDriveItems = true;
            listRequest.TeamDriveId = "0ABCwCqmH5MocUk9PVA";
            listRequest.SupportsTeamDrives = true;
            listRequest.Q = "mimeType contains 'sheet' and name contains 'Honeycomb Dashboard 20'";

            // List files.
            IList<Google.Apis.Drive.v3.Data.File> files = listRequest.Execute()
                .Files;

            return files;
        }

        public void ProcessSpreadsheetDetails(IList<Google.Apis.Drive.v3.Data.File> spreadsheets)
        {
            using (var db = new Honeycomb()) { 
                var spread = new Spreadsheet();
                var files = spreadsheets;
                foreach (var file in files)
                {


                spread.Name = file.Name;
                spread.SheetID = file.Id;
                spread.Location = file.Parents[0];

                    // Check record is not in the database before inserting
                    if (db.Spreadsheets.Any(x => x.SheetID == spread.SheetID && x.Location != spread.Location))// || x.Name != spread.Name))
                    {

                        var toUpdate = new Spreadsheet();
                        toUpdate = db.Spreadsheets.Where(x => x.SheetID == spread.SheetID && x.Name == spread.Name).Select(x => x).FirstOrDefault();
                        toUpdate.Location = spread.Location;
                        toUpdate.Name = spread.Name;
                        db.SaveChanges();
                        
                    }
                    if (!db.Spreadsheets.Any(x => x.SheetID == spread.SheetID))
                    {
                        db.Spreadsheets.Add(spread);
                        db.SaveChanges();
                    }
                   
                
                }
            }
        }
    }
}
