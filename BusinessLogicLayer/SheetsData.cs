﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.IO;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;


namespace BusinessLogicLayer
{
    public class SheetsData
    {
        public int RetrieveSpreadheetsData(string spreadSheetID)
        {
            // Create Drive API service.
            var service = APIConnection.CreateSheetsService();

            // Define request parameters. THIS IS FOR THE USER DATA
            String spreadsheetId = spreadSheetID;//"1WScHF2qkpnpm45ZUujWX_DDkq95l07F1lywTKltAA58";
            String range = "2017 T1 Growth Bonus!A2:H20";
            SpreadsheetsResource.ValuesResource.GetRequest request =
                    service.Spreadsheets.Values.Get(spreadsheetId, range);

            // Prints the names and majors of students in a sample spreadsheet:
            // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
            ValueRange response = request.Execute();
            IList<IList<Object>> employeeValues = response.Values;

            // Define request parameters. THIS IS FOR THE FIRST MONTH export            
            String range1 = "2017 T1 Growth Bonus!I2:AC20";
            SpreadsheetsResource.ValuesResource.GetRequest request1 =
                    service.Spreadsheets.Values.Get(spreadsheetId, range1);

            // Prints the names and majors of students in a sample spreadsheet:
            // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
            ValueRange response1 = request1.Execute();
            IList<IList<Object>> dataValues = response1.Values;

            using (var db = new Honeycomb())
            {
                var parentSheet = new Spreadsheet();
                parentSheet = db.Spreadsheets.Where(x => x.SheetID == spreadSheetID).Select(x => x).FirstOrDefault();
            
            // Call the insert/update methods
            SheetsData.InsertHDEmployee(parentSheet.ID, employeeValues, dataValues);
                return parentSheet.ID;
             }
             
        }
        public static void InsertHDEmployee(int spreadsheetID, IList<IList<Object>> employeeValues, IList<IList<Object>> dataValues)
        {
            // Creation of the HoneycombDashboardEmployee and Data objects for insertion in the DB
            string[] months = { "March", "April", "May", "June" }; // Array to hold the months of the report
            var hDEmployee = new HoneycombDashboardEmployee();
            using (var db = new Honeycomb())
            {

                for (var i = 0; i < employeeValues.Count; i++)
            {
                var row = employeeValues[i];
                // Fill the object with the report data
                hDEmployee.SpreadsheetID = spreadsheetID;
                hDEmployee.FirstName = row[0].ToString();
                hDEmployee.LastName = row[1].ToString();
                hDEmployee.Initials = row[2].ToString();
                hDEmployee.EmailAddress = row[3].ToString();
                hDEmployee.FTStatus = row[4].ToString();
                hDEmployee.Status = row[5].ToString();
                hDEmployee.DateJoined = row[6].ToString();
                hDEmployee.EndOfTrimester = row[7].ToString();

                // Check if it is, then update.
                if (db.HoneycombDashboardEmployees.Any(x => x.EmailAddress == hDEmployee.EmailAddress && x.SpreadsheetID == hDEmployee.SpreadsheetID))
                {
                    var toUpdate = new HoneycombDashboardEmployee();
                    toUpdate = db.HoneycombDashboardEmployees.Where(x => x.EmailAddress == hDEmployee.EmailAddress && x.SpreadsheetID == hDEmployee.SpreadsheetID).Select(x => x).FirstOrDefault();
                    toUpdate.SpreadsheetID = hDEmployee.SpreadsheetID;
                    toUpdate.FirstName = hDEmployee.FirstName;
                    toUpdate.LastName = hDEmployee.LastName;
                    toUpdate.Initials = hDEmployee.Initials;
                    toUpdate.EmailAddress = hDEmployee.EmailAddress;
                    toUpdate.FTStatus = hDEmployee.FTStatus;
                    toUpdate.Status = hDEmployee.Status;
                    toUpdate.DateJoined = hDEmployee.DateJoined;
                    toUpdate.EndOfTrimester = hDEmployee.EndOfTrimester;
                    db.SaveChanges();
                    int interval = 0;
                    foreach (var m in months)
                    {

                            SheetsData.InsertHDData(toUpdate.ID, m, interval, dataValues[i]);
                        interval = interval + 5;
                    }

                }// Check record is not in the database before inserting
                 //if (!db.HoneycombDashboardEmployees.Any(x => x.EmailAddress == hDEmployee.EmailAddress))
                else
                {
                    db.HoneycombDashboardEmployees.Add(hDEmployee);
                    db.SaveChanges();
                    int interval = 0;
                    foreach (var m in months)
                    {

                            SheetsData.InsertHDData(hDEmployee.ID, m, interval, dataValues[i]);
                            interval = interval + 5;
                    }
                }




            }
        }
        }
        public static void InsertHDData(int employeeID, string month, int interval, IList<Object> values)
        {
            var hDData = new HoneycombDashboardData();
            using (var db = new Honeycomb())
            {

                // Prepare the Data row for comparison

                hDData.HonecombDashboardEmployee_ID = employeeID;
            hDData.Month = month;
            hDData.BillableHours = values[0 + interval].ToString() == "" ? (Decimal?)null : System.Convert.ToDecimal(values[0 + interval]);
            hDData.Credits = values[1 + interval].ToString() == "" ? (Decimal?)null : System.Convert.ToDecimal(values[1 + interval]);
            hDData.GoalBillableHours = values[2 + interval].ToString() == "" ? (Decimal?)null : System.Convert.ToDecimal(values[2 + interval]);
            hDData.Reminders = values[3 + interval].ToString() == "" ? (int?)null : System.Convert.ToInt16(values[3 + interval]);
            hDData.SpecialNotes = values[4 + interval].ToString();

            // First review if there is already an entry for this particular month and this user
            if (db.HoneycombDashboardDatas.Any(x => x.HonecombDashboardEmployee_ID == hDData.HonecombDashboardEmployee_ID && x.Month == hDData.Month))
            {
                var toUpdate = new HoneycombDashboardData();
                toUpdate = db.HoneycombDashboardDatas.Where(x => x.HonecombDashboardEmployee_ID == hDData.HonecombDashboardEmployee_ID && x.Month == hDData.Month).Select(x => x).FirstOrDefault();
                toUpdate.BillableHours = hDData.BillableHours;
                toUpdate.Credits = hDData.Credits;
                toUpdate.GoalBillableHours = hDData.GoalBillableHours;
                toUpdate.Reminders = hDData.Reminders;
                toUpdate.SpecialNotes = hDData.SpecialNotes;
                db.SaveChanges();

            }
            // If it does not exist, then create a new entry            
            if (!db.HoneycombDashboardDatas.Any(x => x.HonecombDashboardEmployee_ID == hDData.HonecombDashboardEmployee_ID && x.Month == hDData.Month))
            {
                db.HoneycombDashboardDatas.Add(hDData);

                db.SaveChanges();
            }




        }
        }
    }
}
